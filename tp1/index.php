<?php
 
// Configuration de la grille
 
$grid_width  = 10; // largeur de la grille
$grid_height = 10; // hauteur de la grille
$cell_width  = 25; // largeur d'une cellule
$cell_height = 25; // hauteur d'une cellule
$gutter      = 5;  // espacement des cellules
 
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grid</title>
    <style type="text/css" media="screen">
    .grid .row {
        margin-left: -<?=$gutter?>px;
    }
 
    .grid .row:after {
        content: " ";
        visibility: hidden;
        display: block;
        height: 0;
        clear: both;
    }
 
    .grid .cell {
        display: block;
        float: left;
        margin-left: <?=$gutter?>px;
        margin-bottom: <?=$gutter?>px;
        width: <?=$cell_width?>px;
        height: <?=$cell_height?>px;
        border: 1px solid #ddd;
        border-top: none;
        border-left: none;
    }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript">
    $(function () {
        $('.cell').hover(
            function () {
                $(this).css('background', '#eee');
            },
            function () {
                $(this).css('background', 'white');
            }
        );
    });
    </script>
</head>
<body>
    <div class="grid">
    <? for ($w=0; $w<$grid_width; $w++): ?>
        <div class="row">
        <? for ($h=0; $h<$grid_height; $h++): ?>
            <span class="cell"></span>
        <? endfor ?>
        </div>
    <? endfor ?>
    </div>
</body>
</html>

    