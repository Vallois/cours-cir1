<?php
// Connexion à la base de données
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=Chat;charset=utf8', 'root', 'isencir');
}
catch(Exception $e)
{
        die('Erreur : 0'.$e->getMessage());
}

// Insertion du message à l'aide d'une requête préparée
$req = $bdd->prepare('INSERT INTO Chat (pseudo, message, date) VALUES(?, ?, ?)');
$req->execute(array($_POST['pseudo'], $_POST['message'], $_POST['date']));

// Redirection du visiteur vers la page du minichat
header('Location: Chat.php');
?>
